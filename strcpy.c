#include <stdio.h>

char *scpy(char *desStr, char *srcStr)
{
    char *res = desStr;
    while(*srcStr)
    {
        *desStr++ = *srcStr++;
    }
    return res;
}

int main()
{
    char str1[]="123";
    char str2[]="456";

    scpy(str1, str2);
    printf("%s\n", str1);
    return 0;
}
